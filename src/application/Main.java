package application;

import java.io.EOFException;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.hut.backend.trayIcon.DisplayTrayIcon;
import com.hut.gui.Start.SplashScreenController;
import com.hut.gui.stage.StageUpdater;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.application.Preloader.StateChangeNotification;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
public class Main extends Application {
	public boolean timer()
	{
		try {
			Thread.sleep(3002);
			return true;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public static void newGame()
	{
		
	}
	@Override
	public void start(Stage primaryStage) throws IOException, InterruptedException {
		try 
		{

			SplashScreenController n=new SplashScreenController();



			Stage s=new Stage();
			//remove window decoration
			s.initStyle(StageStyle.UNDECORATED);
			Parent root=FXMLLoader.load(getClass().getResource("../com/hut/gui/Start/SplashScreen.fxml"));
			
			FadeTransition fadein=new FadeTransition(Duration.seconds(4), root);
			fadein.setFromValue(1);
			fadein.setToValue(1);
			fadein.setCycleCount(1);
			fadein.play();
			fadein.setOnFinished(e->
			{
				s.close();
				try {
					
					n.mainScreen(primaryStage);
					DisplayTrayIcon DTI=new DisplayTrayIcon();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			});

			s.setScene(new Scene(root));

			s.show();

		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{

		}
	}


	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {

		
		launch(args);
		

	}
}
