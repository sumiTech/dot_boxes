package com.hut.line;

import java.io.IOException;

import com.hut.gui.game.GameController;
import com.hut.gui.player.PlayerController;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LineCheck {
	public static int turnscount=0;
	public static int countn=0;
	private static int array[]=new int[58];
	private static int boxesLineArray[][] = new int[24][4];
	private static final int selectedLine = -1; 
	private static boolean boxes[] = new boolean[24];
	
	//Patterns 
	//1//2//3//4//5//6
	//7//8//9//10//11//12
	//13//14//15//16//17//18
	//19//20//21//22//23//24//25
	//26//27//28//29//30//31
	//32//33//34//35//36//37//38
	//39//40//41//42//43//44
	//45//46//47//48//49//50//51
	//52//53//54//55//56//57
	
	//Box Patterns
	//1//7//8//14
	//2//8//9//15
	//3//9//10//16
	
	
	//To Initialize each Box with the Given Value of Label
	public static void initializeBox()
	{
		int increamentalValue = 1;
		int startValue = 65;
		for(int i = 0; i < boxesLineArray.length; i++)
		{
			for(int j = 0; j < boxesLineArray[i].length; j++)
			{
				switch(j)
				{
				case 0:
					increamentalValue = 0;
					break;
				case 1:
					increamentalValue = 6;
					break;
				case 2:
					increamentalValue = 7;
					break;
				case 3:
					increamentalValue = 13;
					break;
				default:
					System.out.println(j + ": Error!");
				}
				boxesLineArray[i][j] = startValue + increamentalValue;
				
			}
			if((i + 1) % 6 == 0) //Row of Box are Initialized
			startValue += 8;
			else
			startValue++;
		}
	}
	
	//Set Value of Clicked Line According to the Player in our 2D Array
	public static void setClickedLine(int lineClicked)
	{
		for(int i = 0; i < boxesLineArray.length; i++)
		{
			for(int j = 0; j < boxesLineArray[i].length; j++)
			{
				if(boxesLineArray[i][j] == lineClicked)
					boxesLineArray[i][j] = selectedLine;
			}
		}
	}
	
	//Reset Values of all our Arrays
	public static void resetLine()
	{
		for(int i = 0; i < array.length; i++)
		{
			array[i]=0;
		}
		for(int i = 0; i < boxesLineArray.length; i++)
		{
			for(int j = 0; j < boxesLineArray[i].length; j++)
			{
				
					boxesLineArray[i][j] = 0;
			}
		}
		for(int i = 0; i < boxes.length; i++)
		{
			//System.out.println(boxes[i]);
			boxes[i] = false;
			
		}
		turnscount=0;
		
	}
	//Check If All Boxes are Completed
	public static boolean isGameEnded()
	{
		boolean gameCompleted = true;
		
		for(int i = 0; i < boxes.length; i++)
		{
			//System.out.println(boxes[i]);
			if(boxes[i] == false)
			{
				gameCompleted = false;
				break;
			}
		}
		return gameCompleted;
	}
	
	//Checking if a Box is Completed
	public static int isBoxComplete()
	{
		int doneLine = 0;
		int boxCompleted = -1;
		for(int i = 0; i < boxesLineArray.length; i++)
		{
			//If the Box is Already Completed then don't Check it
			if(boxes[i] == true && i != boxesLineArray.length)
			{
				//Do Nothing
			}
			else
			{
				for(int j = 0; j < boxesLineArray[i].length; j++)
				{
					if(boxesLineArray[i][j] == selectedLine)
					{
						doneLine++;
					}
				}
				if(doneLine == 4) //If all of The Lines of Boxes are Clicked by the Players
				{
						boxes[i] = true;
						boxCompleted = (i + 1);
						break;
				}
				doneLine = 0;
			}
		}
		return boxCompleted;

	}
	
	//To Check if The Lines Selected are Updating
	public static void printBox()
	{
		for(int i = 0; i < boxesLineArray.length; i++)
		{
			System.out.println(i + "th Row");
			for(int j = 0; j < boxesLineArray[i].length; j++)
			{
				System.out.print(boxesLineArray[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static int getCountn() {
		return countn;
	}
	public static void setCountn(int countn) {
		LineCheck.countn = countn;
	}
	public static void setArray(int count,int id)
	{	setCountn(count);
		
		if(count==57)
		{
			for(int i=0;i<58;i++)
			{
				System.out.println(array[i]);

			}
		}
		if(count==0)
		{
			array[0]=id;
			turnscount++;
		}
		if(count>0)
		{
			array[count]=id;
			turnscount++;
		}
		System.out.println(array[count]);
	}
	

	public static boolean linecheck(int id)
	{	
		for(int i=0;i<turnscount;i++)
		{
			if(id==array[i])
			{
				System.out.println("line is already made");
				return false;
			}

		}
		setArray(turnscount,id);
		setClickedLine(id);
		return true;
	}
}
