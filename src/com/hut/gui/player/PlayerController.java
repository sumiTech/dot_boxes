package com.hut.gui.player;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.hut.DBConnect.DBConnect;
import com.hut.backend.ScoreManager;
import com.hut.backend.player.Player;
import com.hut.gui.game.GameController;
import com.hut.line.LineCheck;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class PlayerController implements Initializable {
	@FXML
	private AnchorPane secondRootPane;

	@FXML
	private Button btnPl1Submit;

	@FXML
	private TextField tfPl1Name;

	@FXML
	private AnchorPane panePl1;

	@FXML
	private Label lblPl1Done;

	@FXML
	private Button btnPl1Back;

	@FXML
	private Label lblPl1Label;

	@FXML
	private RadioButton rbPl1Tick;

	@FXML
	private ToggleGroup labelPl1Group;

	@FXML
	private RadioButton rbPl1Cross;

	@FXML
	private RadioButton rbPl1Circle;

	@FXML
	private ImageView imgPl1Label;

	@FXML
	private TextField tfPl2Name;

	@FXML
	private Button btnPl2Submit;

	@FXML
	private AnchorPane panePl2;

	@FXML
	private Label lblPl2Done;

	@FXML
	private Button btnPl2Back;

	@FXML
	private Label lblPl2Label;

	@FXML
	private RadioButton rbPl2Tick;

	@FXML
	private ToggleGroup labelPl2Group;

	@FXML
	private RadioButton rbPl2Cross;

	@FXML
	private RadioButton rbPl2Circle;

	@FXML
	private ImageView imgPl2Label;

	@FXML
	private Button backBtn;

	Player[] players = new Player[2];
	
	@FXML
	void rbLabel(ActionEvent event) {
		RadioButton rb = (RadioButton) event.getSource();
		String rbName = rb.getText();
		String id = rb.getId();
		Boolean isPlayer1 = (id.charAt(4) == '1');

		if (rbName.equals("Tick")) {
			if (isPlayer1) {
			} // imgPl1Label.setImage(Address of Tick Image);
			else {
			} // imgPl2Label.setImage(Address of Tick Image);
		} else if (rbName.equals("Cross")) {
			if (isPlayer1) {
			} // imgPl1Label.setImage(Address of Cross Image);
			else {
			} // imgPl2Label.setImage(Address of Cross Image);
		} else {
			if (isPlayer1) {
			} // imgPl1Label.setImage(Address of Circle Image);
			else {
			} // imgPl2Label.setImage(Address of Circle Image);
		}

	}

	private String name;

	@FXML
	void btnPl1Back(ActionEvent event) {
		enableRadioButton(labelPl1Group);
		checkSelected(labelPl2Group, rbPl1Tick, rbPl1Cross, rbPl1Circle);
		panePl1.setVisible(false);
		panePl1.setDisable(false);
	}

	@FXML
	void btnPl2Back(ActionEvent event) {
		enableRadioButton(labelPl2Group);
		checkSelected(labelPl1Group, rbPl2Tick, rbPl2Cross, rbPl2Circle);
		panePl2.setVisible(false);
		panePl2.setDisable(false);
	}

	void enableRadioButton(ToggleGroup rbGroup) {
		for (Object radioObject : rbGroup.getToggles()) {
			RadioButton rb = (RadioButton) radioObject;
			if (rb.isDisable())
				rb.setDisable(false);
		}

	}

	@FXML
	void back(ActionEvent event) {
		AnchorPane pane;
		try {
			pane = FXMLLoader.load(getClass().getResource("../mainMenu/MainMenu.fxml"));
			secondRootPane.getChildren().setAll(pane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void actionPl1RbGroup(javafx.scene.input.MouseEvent e)
	{
			enableRadioButton(labelPl2Group);
			checkSelected(labelPl1Group, rbPl2Tick, rbPl2Cross, rbPl2Circle);
	}
	@FXML
	public void actionPl2RbGroup(javafx.scene.input.MouseEvent e)
	{
		enableRadioButton(labelPl1Group);
		checkSelected(labelPl2Group, rbPl1Tick, rbPl1Cross, rbPl1Circle);	
	}
	@FXML
	public void btnPl1Submit(ActionEvent event) throws IOException {

		if(tfPl1Name.getText().equals(""))
		{

					tfPl1Name.setPromptText("Please Enter a Name!");
			tfPl1Name.setStyle("-fx-prompt-text-fill: red");
		}
		else
		{
			if (panePl2.isVisible())
			{
				submit();
			}
			else {			
				panePl1.setVisible(true);
				panePl1.setDisable(false);
			}
		}
		
	}

	void checkSelected(ToggleGroup rbGroup, RadioButton rbTick, RadioButton rbCross, RadioButton rbCircle) {
		RadioButton selectedRb = (RadioButton) rbGroup.getSelectedToggle();
		String rbName = selectedRb.getText();

		if (rbName.equals("Tick")) {
			if (rbTick.isSelected()) {
				rbTick.setSelected(false);
				rbCross.setSelected(true);
			}
			rbTick.setDisable(true);
		} else if (rbName.equals("Cross")) {
			if (rbCross.isSelected()) {
				rbCross.setSelected(false);
				rbCircle.setSelected(true);
			}
			rbCross.setDisable(true);
		} else {
			if (rbCircle.isSelected()) {
				rbCircle.setSelected(false);
				rbCross.setSelected(true);
			}
			rbCircle.setDisable(true);
		}
	}

	private void setPlayers()
	{
		int oldPlayer1 = -1;
		int oldPlayer2 = -1;
		
		players[0] = new Player();
		players[1] = new Player();
			
		players[0].setName(tfPl1Name.getText());
		players[1].setName(tfPl2Name.getText());

		String labelPl1 = ((RadioButton)labelPl1Group.getSelectedToggle()).getText();
		String labelPl2 = ((RadioButton)labelPl2Group.getSelectedToggle()).getText();
		
		players[0].setLabel(labelPl1);
		players[1].setLabel(labelPl2);
		
		try{
			DBConnect.getConnection();
			ResultSet rs = DBConnect.getStatement().executeQuery("SELECT NAME FROM PLAYER");
			
			rs.last();
			int rows = rs.getRow();
			
			int totalPlayer = 0;
			System.out.println(rows);
			if(rows != 0)
			{
				totalPlayer = rs.getRow();
			}
			
			rs.beforeFirst();
			
			while(rs.next())
			{
				if(rs.getString(1).equals(tfPl1Name.getText()))
				{
					
					oldPlayer1 = rs.getRow();
					players[0].setId(oldPlayer1);
				}
				else if(rs.getString(1).equals(tfPl2Name.getText()))
				{
					oldPlayer2 = rs.getRow();
					players[1].setId(oldPlayer2);
				}
			}
			System.out.println(totalPlayer);
			if(oldPlayer1 == -1)
			{
				totalPlayer++;
				DBConnect.getStatement().execute("INSERT INTO PLAYER values(" + totalPlayer + ", '" + players[0].getName() + "', 0)");
				players[0].setId(totalPlayer);
			}
			if(oldPlayer2 == -1)
			{
				totalPlayer++;
				DBConnect.getStatement().execute("INSERT INTO PLAYER values(" + totalPlayer + ", '" + players[1].getName() + "', 0)");
				players[1].setId(totalPlayer);
			}
			
			DBConnect.disconnect();
		}catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
	}
	
	@FXML
	public void btnPl2Submit(ActionEvent event) throws IOException 
	{
		if(tfPl2Name.getText().equals(""))
		{

			tfPl2Name.setPromptText("Please Enter a Name!");
			tfPl2Name.setStyle("-fx-prompt-text-fill: red");
		}
		else
		{
			if (panePl1.isVisible())
			{ 
				submit();
			}
			else
			{
				panePl2.setVisible(true);
				panePl2.setDisable(false);
			}
		}
		
	}

	@FXML
	void submit() throws IOException {
		LineCheck.initializeBox();

		
		setPlayers();
		
		GameController controller = new GameController();
		controller.setPlayers(players[0], players[1]);

		FXMLLoader loader = new FXMLLoader(getClass().getResource("../game/Game.fxml"));
		loader.setController(controller);
		AnchorPane pane = (AnchorPane) loader.load();
		secondRootPane.getChildren().setAll(pane);

	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		checkSelected(labelPl1Group, rbPl2Tick, rbPl2Cross, rbPl2Circle);
		checkSelected(labelPl2Group, rbPl1Tick, rbPl1Cross, rbPl1Circle);
	}

}
