package com.hut.gui.MenuBar;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.hut.backend.trayIcon.DisplayTrayIcon;
import com.hut.gui.mainMenu.MainMenuController;
import com.hut.gui.stage.StageUpdater;
import com.hut.line.LineCheck;
import com.sun.javafx.scene.layout.region.Margins;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MenuBarController implements Initializable{

	@FXML
	private AnchorPane BasePane;
	@FXML
	private MenuItem newGame;

	@FXML
	private MenuItem quit;
	@FXML
	private MenuItem about;
	@FXML
	public void about(ActionEvent event) {
		Stage s=new Stage();
		//remove window decoration
		s.initStyle(StageStyle.UNDECORATED);
		try {
			Parent root=FXMLLoader.load(getClass().getResource("../about/about.fxml"));
			s.setScene(new Scene(root));
			s.setX(380);
			s.setY(230);
			s.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@FXML
	private ContextMenu contextMenu;
	@FXML
	public void newGame(ActionEvent event) {
		AnchorPane pane;
		try {
			System.out.println("hello");
			LineCheck.resetLine();
			pane = FXMLLoader.load(getClass().getResource("../mainMenu/MainMenu.fxml"));
			BasePane.getChildren().setAll(pane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void quit(ActionEvent event) 
	{

		System.out.println("quiting");
		DisplayTrayIcon.trayn.remove(DisplayTrayIcon.getTrayicon()); 
		System.out.println("executed");
		// get a handle to the stage
		// do what you have to do
		Stage s=(Stage)BasePane.getScene().getWindow();
		s.close();
	}

	@FXML
	public void createStage()throws IOException
	{

	}
	@FXML
	public void defaultTheme()
	{
		for(UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
		{

			if("System".equals(info.getName()))
			{
				try {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
					System.out.println(ex);
				}
			}
		}
	}

	@FXML
	public void metalTheme()
	{
		for(UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
		{

			if("Metal".equals(info.getName()))
			{
				try {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
					System.out.println(ex);
				}
			}
		}
		BasePane.requestFocus();
	}


	@FXML
	public void motifTheme()
	{
		for(UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
		{

			if("Motif".equals(info.getName()))
			{
				try {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
					System.out.println(ex);
				}
			}
		}
	}

	@FXML
	public void gtkTheme()
	{
		for(UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
		{

			if("GTK".equals(info.getName()))
			{
				try {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
					System.out.println(ex);
				}
			}
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		StageUpdater.setN(BasePane);

	}

}
