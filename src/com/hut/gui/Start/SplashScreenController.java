package com.hut.gui.Start;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.hut.gui.stage.StageUpdater;

import application.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class SplashScreenController implements Initializable {
	public static Stage last;
	public static Stage getLast() {
		return last;
	}
	public static void setLast(Stage last) {
		SplashScreenController.last = last;
	}

	@FXML
	private ImageView SplashVideo;
	
	public boolean start2(Stage primaryStage) throws IOException, InterruptedException {
		Stage s=new Stage();
		//remove window decoration
		s.initStyle(StageStyle.UNDECORATED);
		Parent root=FXMLLoader.load(getClass().getResource("../Start/SplashScreen.fxml"));

		s.setScene(new Scene(root));
		s.show();
		Thread.sleep(3002);
		s.close();
		return true;
	}
	public void mainScreen(Stage primaryStage) throws IOException, InterruptedException
	{
		primaryStage.getIcons().add(new Image("file:C:/Users/Usman/Documents/dot_boxes/src/com/hut/icons/Logo.png"));
			
		Parent root=FXMLLoader.load(getClass().getResource("../MenuBar/menuBar.fxml"));
		Scene scene=new Scene(root);
		primaryStage.setTitle("DOT&BOX");
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		AnchorPane pane=FXMLLoader.load(getClass().getResource("../mainMenu/MainMenu.fxml"));
		StageUpdater.getN().getChildren().setAll(pane);
		
		primaryStage.show();


	}	
	public boolean timer()
	{

		return true;
	}

	public void end2(Stage primaryStage)
	{
		primaryStage.close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub


	}

}
