package com.hut.gui.highScore;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import com.hut.DBConnect.DBConnect;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
//import net.proteanit.sql.DbUtils;

public class HighScore implements Initializable {

	@FXML
	private GridPane gPane;

	@FXML
	private AnchorPane highScorePain;

	@FXML
	private TableView<PLAYER> table;

	@FXML
	private TableColumn<PLAYER,String> Namefield;
	  @FXML
	    private Button backBtn;

	    @FXML
	    void back(ActionEvent event) {

	    	AnchorPane p;
			try {
				p = FXMLLoader.load(getClass().getResource("../mainMenu/MainMenu.fxml"));
				highScorePain.getChildren().setAll(p);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	@FXML
	private TableColumn<PLAYER, String> noOfWins;
	private static Statement st;
	private static String Name;
	private static int NoOfWins2;

	static int getNoOfPlayer()
	{
		int noofrecords=0;
		DBConnect.getConnection();
		try {
			ResultSet r=DBConnect.getStatement().executeQuery("Select * from PlAYER");
			while(r.next())
			{

				noofrecords++;

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnect.disconnect();
		return noofrecords;
	}
	public String[][] getRecord()
	{int i=0;
	String a[][]=new String [getNoOfPlayer()+1][2];
	System.out.println("in");
	DBConnect.getConnection();
	try {
		ResultSet r=DBConnect.getStatement().executeQuery("select * from PLAYER order by wins desc");
		while(r.next())
		{

			a[i][0]=r.getString("NAME");
			a[i][1]=r.getInt("WINS")+"";
			System.out.println(a[i][0]);
			i++;
		}

	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	return a;

	}
	void dataDisplay()
	{
		int i = 0;
		int j = 0;
		String a[][]=new String[getNoOfPlayer() + 1
		                        ][];
		a=getRecord();

		for(Object lbl : gPane.getChildren())
		{
			Label lb = (Label)lbl;
			System.out.println(i + ", " + j);
			lb.setText(a[i][j]);
			if(j % 2 != 0)
				{i++;j=0;}
			else j++;
			
			if(i == 2)
					break;
		}

	}





	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		System.out.println("inside high score");
		dataDisplay();
	}

}
