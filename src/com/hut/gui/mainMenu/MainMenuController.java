package com.hut.gui.mainMenu;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.hut.backend.trayIcon.DisplayTrayIcon;
import com.hut.gui.game.GameController;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class MainMenuController implements Initializable {
	@FXML
	private AnchorPane rootPane;

	@FXML
	private Button btnStart;

	@FXML
	private Button btnQuit;
	
	@FXML
    private Rectangle styleLine;
	
	@FXML
    private Button btnHelp;
	
	@FXML
	private Button btnHighScore;
	
	@FXML
	void hoverStart(MouseEvent event) {
		/*DropShadow shadow = new DropShadow();
		btnStart.setEffect(shadow);*/
		Button hoverButton = (Button)event.getSource();
		hoverButton.setStyle("-fx-background-radius: 30%;-fx-background-color: #0a79a6; -fx-border-color: white; -fx-border-radius: 15%");
	}
	@FXML
	void hoverEnd(MouseEvent event) {
		/*DropShadow shadow = new DropShadow();
		btnStart.setEffect(shadow);*/
		Button hoverButton = (Button)event.getSource();
		hoverButton.setStyle("-fx-background-radius: 30%;-fx-background-color: #002c3e; -fx-border-color: white; -fx-border-radius: 15%");
	}

	@FXML
	void startbtn(ActionEvent event) throws IOException {
		AnchorPane pane=FXMLLoader.load(getClass().getResource("../player/Player.fxml"));
		rootPane.getChildren().setAll(pane);
	}
	@FXML
	void HighScore(ActionEvent event) throws IOException {
		AnchorPane pane=FXMLLoader.load(getClass().getResource("../highScore/HighScore.fxml"));
		rootPane.getChildren().setAll(pane);
	}
	
	@FXML
	void helpBtn(ActionEvent event){
		System.out.println("Help!");
	}
	
	@FXML
	public
	void quitstage(ActionEvent event){
		System.out.println("quiting");
		DisplayTrayIcon.trayn.remove(DisplayTrayIcon.getTrayicon()); 
		System.out.println("executed");
		// get a handle to the stage
		Stage stage = (Stage) btnQuit.getScene().getWindow();
		// do what you have to do
		stage.close();
	}
	
	@FXML
	void helpPopup(MouseEvent event) {
		GameController g =new GameController();
		g.help(event);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	

	}

}
