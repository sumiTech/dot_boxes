package com.hut.gui.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Timer;

import javax.swing.JOptionPane;

import com.hut.backend.ScoreManager;
import com.hut.backend.player.Player;
import com.hut.gui.MenuBar.MenuBarController;
import com.hut.gui.player.PlayerController;
import com.hut.gui.winner.WinnerController;
import com.hut.line.LineCheck;
import com.sun.javafx.event.EventQueue;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.ImageViewBuilder;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class GameController implements Initializable {
	@FXML
	private AnchorPane gameRootPane;

	@FXML
	private ContextMenu contextMenu;

	@FXML
	private Button lineA;

	@FXML
	private Button lineB;

	@FXML
	private Button lineC;

	@FXML
	private Button lineN;

	@FXML
	private Button lineO;

	@FXML
	private Button lineP;

	@FXML
	private Button lineA998;

	@FXML
	private Button lineB998;

	@FXML
	private Button lineC998;

	@FXML
	private Button lineh;

	@FXML
	private Button linei;

	@FXML
	private Button linej;

	@FXML
	private Button lineG;

	@FXML
	private Button lineH;

	@FXML
	private Button lineI;

	@FXML
	private Button lineJ;

	@FXML
	private Button lineT;

	@FXML
	private Button lineU;

	@FXML
	private Button lineV;

	@FXML
	private Button lineW;

	@FXML
	private Button linea;

	@FXML
	private Button lineb;

	@FXML
	private Button linec;

	@FXML
	private Button lined;

	@FXML
	private Label box2Label;

	@FXML
	private Label box3Label;

	@FXML
	private Button lineD;

	@FXML
	private Button lineE;

	@FXML
	private Button lineF;

	@FXML
	private Button lineQ;

	@FXML
	private Button lineR;

	@FXML
	private Button lineS;

	@FXML
	private Button lineD998;

	@FXML
	private Button lineE998;

	@FXML
	private Button lineF998;

	@FXML
	private Button linek;

	@FXML
	private Button linel;

	@FXML
	private Button linem;

	@FXML
	private Button lineK;

	@FXML
	private Button lineL;

	@FXML
	private Button lineM;

	@FXML
	private Button lineX;

	@FXML
	private Button lineY;

	@FXML
	private Button lineZ;

	@FXML
	private Button linee;

	@FXML
	private Button linef;

	@FXML
	private Button lineg;

	@FXML
	private Button lineu;

	@FXML
	private Button linev;

	@FXML
	private Button linew;

	@FXML
	private Button linen;

	@FXML
	private Button lineo;

	@FXML
	private Button linep;

	@FXML
	private Button lineq;

	@FXML
	private Button linex;

	@FXML
	private Button liney;

	@FXML
	private Button linez;

	@FXML
	private Button liner;

	@FXML
	private Button lines;

	@FXML
	private Button linet;

	@FXML
	private Label lblStatusBar;

	@FXML
	private Label lblPl1Name;

	@FXML
	private Label lblPl2Name;

	@FXML
	private ImageView help;

	@FXML
	private GridPane gridPane;

	@FXML
	private Button backBtn;
	@FXML
	private ImageView boxCom1;

	@FXML
	private ImageView boxCom2;

	@FXML
	private ImageView boxCom3;

	@FXML
	private ImageView boxCom4;

	@FXML
	private ImageView boxCom5;

	@FXML
	private ImageView boxCom6;

	@FXML
	private AnchorPane BoxMakePane;

	@FXML
	private Label lblPl1Score;

	@FXML
	private Label lblPl2Score;

	@FXML
	private Label lblPl1Turn;

	@FXML
	private Label lblPl2Turn;
	@FXML
	private MenuItem NewGame;

	@FXML
	private MenuItem About;

	@FXML
	private MenuItem Quit;

	@FXML
	void newGame(ActionEvent event) 
	{
		AnchorPane pane;
	
		if(LineCheck.turnscount!=0)
		{
			if(playerTurnCount%2==0)
			{
				System.out.println("player 2 Wins");
			}
			else
			{
				System.out.println("player 1 Wins");
			}
		}
		else{
			LineCheck.resetLine();

			try {

				pane = FXMLLoader.load(getClass().getResource("../mainMenu/MainMenu.fxml"));
				gameRootPane.getChildren().setAll(pane);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@FXML
	void quit(ActionEvent event) 
	{
		MenuBarController mbc=new MenuBarController();
		Stage s=(Stage)gameRootPane.getScene().getWindow();
		s.close();
	}
	@FXML
	void about(ActionEvent event) 
	{
		MenuBarController mbc=new MenuBarController();
		mbc.about(event);
	}
	String playerImageUrl;
	String pl1Name,pl2Name;

	int pl1Score, pl2Score;
	int playerTurnCount = 0;

	//For Current Values of a Player
	int currentPlayer; 
	String currentColor;
	String currentName;
	Label lblCurrentScore;
	int currentScore;
	Label lblCurrentTurn;
	String pl1Label;
	String pl2Label;
	
	
	Player player1;
	Player player2;

	//Assigns The First Turn, It Calculates at Random which Player Should be given First Turn.
	void setFirstTurn()
	{
		Random range = new Random();
		playerTurnCount = range.nextInt(2);

		lblStatusBar.setText("Player " + (playerTurnCount + 1) + " Turn");
	}

	//Updates the Status Bar
	//The Parameter text Passed is Set in lblStatusBar
	private void updateStatus(String text)
	{
		lblStatusBar.setText(text);
	}

	//Updates the Score
	//1st Parameter - The Label of Current Player's Score
	//2nd Parameter - The Score to be Updated
	private void updateScore(Label label, int score)
	{System.out.println("Updating Score : " + score);
		label.setText("" + score);
	}

	//Shows the Celebration Gif for 3 Seconds
	//Parameter - The Object of ImageView to be Shown
	public void setCompleteAnimation(ImageView celebrationGif)
	{
		System.out.println("Celebration");
		celebrationGif.setImage(new Image("file:C:/Users/Usman/Documents/dot_boxes/src/com/hut/icons/CelebrationGif2.gif"));
		BoxMakePane.getChildren().add(celebrationGif);

		FadeTransition fadein=new FadeTransition(Duration.seconds(3), celebrationGif);

		fadein.setFromValue(1);
		fadein.setToValue(1);
		fadein.setCycleCount(1);
		fadein.play();

		fadein.setOnFinished(e->
		{

			BoxMakePane.getChildren().remove(celebrationGif);
			celebrationGif.setVisible(false);
		});
	}

	//To Show the Images when a Box is Completed
	//Parameter - The Box Number that is Completed 
	public void completeBox(int boxNumber)
	{
		int i = 1;
		for(Object child: BoxMakePane.getChildren())
		{
			if(i == boxNumber)
			{
				ImageView boxImage=(ImageView)child;
				ImageView gif = new ImageView();

				gif.setFitWidth(boxImage.getFitWidth());
				gif.setFitHeight(boxImage.getFitHeight());
				gif.setLayoutX(boxImage.getLayoutX() - 5);
				gif.setLayoutY(boxImage.getLayoutY());

				setCompleteAnimation(gif);
System.out.println(playerImageUrl);
				Image image = new Image ("file:" + playerImageUrl);
				boxImage.setImage(image);


				FadeTransition fadein=new FadeTransition(Duration.seconds(1.5), boxImage);
				fadein.setFromValue(0);
				fadein.setToValue(1);
				fadein.setCycleCount(1);
				fadein.play();


				break;
			}

			i++;
		}

	}
	FadeTransition fade = null;
	public void blinkPlayerTurnIcon(Label imgTurn)
	{
		if(fade != null)
		{
			fade.stop();
			Label lbl = (Label)fade.getNode();
			lbl.setOpacity(1.0);
		}
		fade = new FadeTransition(Duration.seconds(0.75), imgTurn);
		fade.setAutoReverse(true);
		fade.setFromValue(0.0);
		fade.setToValue(1.0);
		fade.setCycleCount(Animation.INDEFINITE);
		fade.play();

	}

	//Set Players Name in the Local Variable of this Class Using the Object of Player Class from PlayerController
	//Parameter 1 - Player 1 Object
	//Parameter 2 - Player 2 Object
	public void setPlayers(Player player1, Player player2)
	{
		this.player1 = player1;
		this.player2 = player2;

		pl1Name = player1.getName();
		pl2Name = player2.getName();

		pl1Score = player1.getScore();
		pl2Score = player2.getScore();
		
		pl1Label = player1.getLabel();
		pl2Label = player2.getLabel();
		
	}

	//Sets the Values of Our All 'Current Variables' of Present Player's Turn
	public void setPlayerEnvironment()
	{
		if(playerTurnCount % 2 == 0) //Player 1 Turn
		{
			playerImageUrl = "C:/Users/Usman/Documents/dot_boxes/src/com/hut/icons/" + pl1Label + ".png";
			currentPlayer = 1;
			currentName = pl1Name;
			currentColor = "red";
			lblCurrentScore = lblPl1Score;
			currentScore = Integer.parseInt(lblPl1Score.getText());
			lblCurrentTurn = lblPl1Turn;
		}
		else	//Player 2 Turn
		{
			playerImageUrl = "C:/Users/Usman/Documents/dot_boxes/src/com/hut/icons/" + pl2Label + ".png";
			currentPlayer = 2;
			currentName = pl2Name;
			currentColor = "blue";
			lblCurrentScore = lblPl2Score;
			currentScore = Integer.parseInt(lblPl2Score.getText());
			lblCurrentTurn = lblPl2Turn;
		}	
	}

	//Ends the Game, Announces the Winner and Loads the Winning Screen
	public void endGame()
	{
		Player winnerPlayer = null;
		player1.setScore(Integer.parseInt(lblPl1Score.getText()));
		player2.setScore(Integer.parseInt(lblPl2Score.getText()));
		//Compare the Score of Player 1 and Player 2
		if(Integer.parseInt(lblPl1Score.getText()) > Integer.parseInt(lblPl2Score.getText()))
			winnerPlayer = player1;
		else if(Integer.parseInt(lblPl1Score.getText()) < Integer.parseInt(lblPl2Score.getText()))
			winnerPlayer = player2;
		else 
		{
			winnerPlayer = new Player();
			winnerPlayer.setName("Draw");
			winnerPlayer.setScore(player1.getScore());
		}
		

		WinnerController winController = new WinnerController();
		winController.setWinner(winnerPlayer);
		AnchorPane pane;
		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource("../winner/Winner.fxml"));
			loader.setController(winController);
			pane = (AnchorPane) loader.load();
			gameRootPane.getChildren().setAll(pane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void back(ActionEvent event) {
		AnchorPane pane;
		try {
			pane=FXMLLoader.load(getClass().getResource("../player/Player.fxml"));
			gameRootPane.getChildren().setAll(pane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void help(MouseEvent event) {

		Stage s=new Stage();
		//remove window decoration
		s.initStyle(StageStyle.UNDECORATED);
		try {
			Parent root=FXMLLoader.load(getClass().getResource("../help/Help.fxml"));
			s.setScene(new Scene(root));
			s.centerOnScreen();
			s.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	//Insert the Players Name and Score in their Respective Labels
	void insertPlayerAtt(ActionEvent e)
	{
		lblPl1Name.setText(pl1Name);
		lblPl2Name.setText(pl2Name);

		lblPl1Score.setText("" + pl1Score);
		lblPl2Score.setText("" + pl2Score);
	}


	@FXML
	//When a Line is Clicked
	//
	void drawline(ActionEvent e) throws IOException
	{

		Button button = (Button)e.getSource();
		String strButtonId = button.getId();

		int c = 0;
		if(strButtonId.length() == 5)
			c = strButtonId.charAt(4);
		else
		{
			c = strButtonId.charAt(4) + (strButtonId.charAt(5) - '0') + (strButtonId.charAt(6) - '0') + (strButtonId.charAt(7) - '0');
		}



		updateStatus("Last Line : " + (c - 64) + " By Player " + currentPlayer + " : " + currentName + ".");

		if(LineCheck.linecheck(c))
		{
			int boxNumber;
			button.setStyle("-fx-background-color: " + currentColor);

			int boxCompleted = 0;
			while(true)
			{
				//Check if The Line Selected has Completed a New Box
				if((boxNumber = LineCheck.isBoxComplete()) != -1)
				{
					System.out.println("Box Completed");

					updateStatus("No. " + boxNumber + " Box Completed By Player " + currentPlayer + " : " + currentName + ".");
					updateScore(lblCurrentScore, Integer.parseInt(lblCurrentScore.getText()) + 1);
					completeBox(boxNumber);
					boxCompleted++;
				}
				else
				{
					if(boxCompleted == 0)
					playerTurnCount++;
					
					break;
				}
			}
			

			//Now Checking if All Boxes have Been Completed and the Game is Ended
			if(LineCheck.isGameEnded())
			{
				endGame(); //Call to endGame Function, to Check Winner and Show The Winner
			}
		}
		else
		{
			updateStatus("Line : " + (c - 64) + " has already been made. Choose another one");
		}

		setPlayerEnvironment();

		blinkPlayerTurnIcon(lblCurrentTurn);
		//setTurnIcon();

	}



	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ActionEvent e=null;

		insertPlayerAtt(e);

		setFirstTurn();
		setPlayerEnvironment();
		blinkPlayerTurnIcon(lblCurrentTurn);

		//setTurnIcon();

	}

}
