package com.hut.gui.winner;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.hut.DBConnect.DBConnect;
import com.hut.backend.player.Player;
import com.hut.backend.winner.Winner;
import com.hut.gui.game.GameController;
import com.hut.line.LineCheck;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class WinnerController implements Initializable {

	@FXML
	private AnchorPane winnerRootPane;

	@FXML
	private Button MainMenu;
	
	@FXML
	private ImageView help;

	@FXML
	private Label lblWinnerPlayer;

	@FXML
	private Label lblWinnerScore;
	
	private Player winner;
	
	Winner win = new Winner();
	@FXML
	void help3(MouseEvent event) {
	    
		GameController g =new GameController();
		g.help(event);
	}
	    
	@FXML
	void mainmenu(ActionEvent event) throws IOException {
		AnchorPane pane;
		try {
			LineCheck.resetLine();
			pane = FXMLLoader.load(getClass().getResource("../mainMenu/MainMenu.fxml"));
			winnerRootPane.getChildren().setAll(pane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateWins()
	{
		DBConnect.getConnection();
		ResultSet rs;
		try {
			rs = DBConnect.getStatement().executeQuery("SELECT WINS FROM PLAYER WHERE ID = " + winner.getId());
			rs.next();
			int a = DBConnect.getStatement().executeUpdate("UPDATE PLAYER SET WINS = " + (rs.getInt(1) + 1 ) + " WHERE ID = " + winner.getId());
			System.out.println(winner.getId());
			if(a < 1){System.out.println("Updated Successfully!");}
			else{System.out.println("Error While Updating!");}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void setEnvironment()
	{
		if(winner.getName().equals("Draw"))
		{
			lblWinnerPlayer.setText(winner.getName());
			lblWinnerScore.setText("Boxes : " + winner.getScore());
		}
		else
		{
			lblWinnerPlayer.setText(winner.getName() + " Wins");
			lblWinnerScore.setText("Boxes : " + winner.getScore());
			win.updateWins(winner);
		}
		
	}
	
	public void setWinner(Player winner)
	{
		this.winner = winner;
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setEnvironment();
		
	}

}
