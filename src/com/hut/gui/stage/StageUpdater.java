package com.hut.gui.stage;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import application.Main;
import javafx.animation.FadeTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;


public class StageUpdater {
	static AnchorPane n;
	

	public static AnchorPane getN() {
		return n;
	}

	public static void setN(AnchorPane n) {
		StageUpdater.n = n;
	}
	
}
