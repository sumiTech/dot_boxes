package com.hut.gui.about;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class AboutController implements Initializable {
	  @FXML
	    private Button close;

	    @FXML
	    void close(ActionEvent event) {
	    	Stage stage = (Stage) close.getScene().getWindow();
	        
	        stage.close();
	    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
