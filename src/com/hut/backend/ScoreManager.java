package com.hut.backend;

public class ScoreManager {
	int score;
	
	public ScoreManager()
	{
		score = 0;
	}
	public int getScore()
	{
		return score;
	}
	
	public void setScore(int score)
	{
		this.score = score;
	}

}
