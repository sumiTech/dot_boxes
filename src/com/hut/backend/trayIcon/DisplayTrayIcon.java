package com.hut.backend.trayIcon;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.plaf.PopupMenuUI;

import com.hut.gui.mainMenu.MainMenuController;
import com.hut.gui.stage.StageUpdater;
import com.sun.glass.ui.Window;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;



public class DisplayTrayIcon {
	static TrayIcon trayicon;
	public static SystemTray trayn;

	public DisplayTrayIcon()
	{
		ShowTrayIcon();
	}
	public static void ShowTrayIcon()
	{
		if(!SystemTray.isSupported())
		{
			System.out.println("Error");
			System.exit(0);
			return;
		}
		final PopupMenu menu=new PopupMenu();

		trayicon= new TrayIcon(CreateIcon("../../icons/Logo2.png", "desc"));
		setTrayicon(trayicon);
		final SystemTray tray=SystemTray.getSystemTray();
		DisplayTrayIcon.trayn=tray;
		trayicon.setPopupMenu(menu);
		trayicon.setToolTip("DOTBox Made BY HUT");
		trayicon.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				AnchorPane n=StageUpdater.getN();
				Stage s=(Stage)n.getScene().getWindow();
				if(n.getOpacity()!=0)
				{
					n.setOpacity(0);
					
				
				Platform.runLater(new Runnable() {
			        @Override
			        public void run() {
			        	stagehide(s);
			          //javaFX operations should go here
			        }
			   });
				}
				else
				{
					n.setOpacity(1);
					s.setResizable(false);
					Platform.runLater(new Runnable() {
				        @Override
				        public void run() {
				        	s.setMaximized(true);
				        	//s.setFullScreen(false);
				        	s.sizeToScene();
				        	s.centerOnScreen();
				        	System.out.println("222");
				          //javaFX operations should go here
				        }
				   });
				}
				System.out.println("tASK bAR cLICKED");		}
		});
		try {
			tray.add(trayicon);


		} catch (AWTException e) {

			e.printStackTrace();
		}
	}	
	
	@FXML
	public static void stagehide(Stage s)
	{
		s.setIconified(true);
	}
	public static TrayIcon getTrayicon() {
		return trayicon;
	}
	public static void setTrayicon(TrayIcon trayicon) {
		DisplayTrayIcon.trayicon = trayicon;
	}
	public static void quiting()
	{

	}
	protected static Image CreateIcon(String path,String desc)
	{
		URL ImageURL=DisplayTrayIcon.class.getResource(path);

		return (new ImageIcon(ImageURL,desc)).getImage();

	}
}
